let portfolio_items = document.querySelector('.portfolio-items');
let portfolio_navigation_links = document.querySelectorAll(
  '.portfolio-navigation li'
);
//Theme-end
let theme_toggle = document.querySelector('.theme-btn');
let theme_toggle_icon = document.querySelector('.theme-btn i');
let get_theme = localStorage.getItem('theme');

if (get_theme !== null) {
  theme_toggle_icon.setAttribute('class', 'ri-sun-line');
  document.documentElement.setAttribute('theme', get_theme);

} else {
  theme_toggle_icon.setAttribute('class', 'ri-moon-clear-line');
}

theme_toggle.addEventListener('click', (e) => {
  e.preventDefault();
  if (!document.documentElement.hasAttribute('theme')) {
    theme_toggle_icon.setAttribute('class', 'ri-sun-line');
    document.documentElement.setAttribute('theme', 'dark');
    localStorage.setItem('theme', 'dark');
  } else {
    theme_toggle_icon.setAttribute('class', 'ri-moon-clear-line');
    document.documentElement.removeAttribute('theme');
    localStorage.removeItem('theme');
  }
});
//Theme-end
let tabs = document.querySelectorAll('.tab');
portfolio_navigation_links.forEach((link) => {
  link.addEventListener('click', () => {
    let select_link = link.getAttribute('data-tab-link');
    portfolio_navigation_links.forEach((link) => {
      link.classList.remove('active');
    });
    link.classList.add('active');
    tabs.forEach((tab) => {
      let active_tab = tab.getAttribute('data-tab');
      if (select_link === active_tab) {
        tabs.forEach((tab) => {
          tab.classList.remove('tab-active');
        });
        tab.classList.add('tab-active');
      }
    });
  });
});
portfolio_items.innerHTML = 'loading...';
async function loadPortfolio(url) {
  let request = await fetch(url);
  if (request.ok) {
    let data = await request.json();
    let list_of_portfolio = data['project'];
    portfolio_items.innerHTML = '';
    for (let i = 0; i < list_of_portfolio.length; i++) {
      let { image, description, title, link, id } = list_of_portfolio[i];
      portfolio_items.innerHTML += `
     <div class="portfolio-item">
     <div class="portfolio-img">
       <img src="${image}" alt="Portfolio" />
     </div>
     <div class="portfolio-overlay">
       <h3 class="portfolio-heading">${
         title.length > 20 ? title.substring(0, 20).concat('...') : title
       }</h3>
       <p class="short-description">
       ${
         description.length > 40
           ? description.substring(0, 40).concat('...')
           : description
       }
       </p>
       <a href="${link}" target="_portfolio" title="click link"><i class="ri-link"></i></a>
     </div>
   </div>
     `;
    }
  } else {
    console.log('No Portfolio Found');
  }
}
loadPortfolio('./data.json');
document.querySelector('.year').innerText = `${new Date().getFullYear()}`;